---
title: Start
body_classes: 'title-center title-h1h2'
---

# Stowarzyszenie Wspólne Oparcie

Tworzymy wspólne oparcie bo pragniemy pomagać skrzywdzonym osobom. Wiemy, że do tego jest potrzebna wspólnota. Stowarzyszenie, jest dla nas narzędziem, które chcemy wykorzystać i udostępniać w celu pomocy wzajemnej.

---

### Tutaj znajdziecie Pomoc

(między innymi: telefony kryzysowe, linki pomocowe)

---

Jeśli jesteście w sytuacji wymagającej wsparcia, chęcią wspólnego działania, piszcie do nas na adres: 
info@wspolneoparcie.org

---

Można nas wesprzeć, najlepiej pieniężnie za pomocą przelewu tradycyjnego:

Numer konta: 75 2530 0008 2041 1073 8230 0001

W tytule przelewu proszę o wpisanie „Darowizna na cele statutowe Stowarzyszenia Wspólne Oparcie”.

---

Gdzie nas znaleźć:

NGO.pl: spis.ngo.pl/392237-stowarzyszenie-wspolne-oparcie-392237

Facebook: facebook.com/wspolneoparcie

Instagram: instagram.com/wspolneoparcie/

Twitter: twitter.com[]()/wspolneoparcie

Mastodon: kolektiva.social/@wspolneoparcie

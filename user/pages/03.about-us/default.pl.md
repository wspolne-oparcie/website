---
title: 'O nas'
media_order: Stowarzyszenie-Wspolne-Oparcie-statut.pdf
---

# O nas

Stowarzyszenie Wspólne Oparcie powstało z doświadczeń pomocy wzajemnej osobom pokrzywdzonym przez system i służby mundurowe w Polsce, a także wielu innych działań w nieformalnych kolektywach. Wynikło że potrzebujemy dodatkowego narzędzia umożliwiającego nam pomoc, ściągnięcie odpowiedzialności z jednostek i większe bezpieczeństwo, tym narzędziem jest formalne stowarzyszenie. Stowarzyszenie jest dla nas narzędziem, które chcemy wykorzystać i udostępniać osobom i kolektywom w celu pomocy wzajemnej.

Tworzymy Wspólne Oparcie bo chcemy być oparciem dla skrzywdzonych osób, wiemy że aby tego dokonać potrzebna jest wspólnota. Większość naszej pracy to niewidoczna z zewnątrz, przyziemna pomoc wzajemna. Jednak oprócz niej, mamy wiele planów na przyszłe działania.

To co chcemy robić całkiem dobrze tłumaczą dwa paragrafy z naszego statutu:

**§ 5 Celem działania Stowarzyszenia jest: **

1. Pomoc społeczna, w tym pomoc rodzinom i osobom w trudnej sytuacji życiowej oraz wyrównywanie szans tych rodzin i osób.
2. Udzielanie osobom w sytuacjach trudnych i kryzysowych, wsparcia oraz pomocy w organizacji specjalistycznej pomocy: psychologicznej, socjalnej, prawnej, medycznej, terapeutycznej.
3. Upowszechnianie i ochrona wolności i praw człowieka oraz swobód obywatelskich.
4. Działalność na rzecz integracji i reintegracji społecznej i zawodowej osób zagrożonych wykluczeniem społecznym i wykluczonych.
5. Wspieranie osób i grup zagrożonych wykluczeniem społecznym i wykluczonych w samoorganizacji i włączanie ich w projektowanie i realizowanie działań na rzecz poprawy ich sytuacji.
6. Promocja i organizacja wolontariatu.
7. Działalność w zakresie kultury, sztuki, ochrony dóbr kultury i dziedzictwa narodowego.
8. Prowadzenie działalności na rzecz ogółu społeczności, w szczególności obrona praw grup dyskryminowanych i zagrożonych wykluczeniem ze względu na niepełnosprawność, wiek, płeć, orientacje i bezrobocie.
9. Udzielenie wsparcia rodzinom i opiekunom osób po kryzysach psychicznych.
10. Edukacja społeczeństwa i przełamywanie stereotypów na temat chorób i zaburzeń psychicznych.
11. Promowanie świadomości związanej z ekologią.

**§ 6 Stowarzyszenie realizuje swoje cele poprzez:**

1. Udzielanie bezpośredniej pomocy osobom będącym w sytuacji trudnej i kryzysowej – organizowanie konsultacji w zakresie m. in. pomocy społecznej, prawnej, oświatowej, zdrowotnej i psychologicznej. Pomoc w dotarciu do miejsc uzyskania pomocy.
2. Organizowanie pomocy materialnej dla osób odbiorczych działań.
3. Przygotowywanie i kolportaż ulotek oraz innych wydawnictw promujących cele Stowarzyszenia.
4. Działalność publicystyczną, informacyjną i wydawniczą; rozpowszechnianie i tłumaczenie materiałów dotyczących obszarów związanych z celami statutowymi.
5. Prowadzenie działalności edukacyjnej i informacyjnej wynikającej z celów statutowych.
6. Współpraca z wszelkimi osobami oraz instytucjami o podobnych celach działania, współpraca z ośrodkami naukowymi i dydaktycznymi, wspieranie i współdziałanie z organizacjami pozarządowymi w Polsce i za granicą.
7. Monitorowanie i opiniowanie działań administracji publicznej, samorządowej, służb mundurowych i podmiotów odpowiedzialnych za opiekę zdrowotną.
8. Występowanie z wnioskami i opiniami do właściwych władz i urzędów oraz sądów w sprawach objętych przedmiotem działania Stowarzyszenia.
9. Przygotowywanie i branie udział w protestach związanych z celami stowarzyszenia. Prowadzenia dla osób z niepełnosprawnością psychiczną form indywidualnego wsparcia w samodzielnym życiu.
10. Podnoszenie kwalifikacji pracowników i wolontariuszy pracujących z osobami w kryzysie, dotkniętymi chorobami psychicznymi, zagrożonych bezrobociem i wykluczeniem społecznym.
11. Pozyskiwanie funduszy ze składek członkowskich, darowizn, spadków, zapisów, zbiórek publicznych, dochodów z majątków Stowarzyszenia oraz ofiarności publicznej i przekazywanie ich na wykonywanie działań wynikających z celów stowarzyszenia.

Cały statut dostępny jest tutaj: [Stowarzyszenie-Wspolne-Oparcie-statut.pdf](Stowarzyszenie-Wspolne-Oparcie-statut.pdf)

Jeśli jesteście w sytuacji wymagającej wsparcia, chęcią wspólnego działania, piszcie do nas na adres: info@wspolneoparcie.org

Możecie nas również wesprzeć pieniężnie: 75 2530 0008 2041 1073 8230 0001

Wpłacając, proszę użyj tytułu „Darowizna na cele statutowe Stowarzyszenia Wspólne Oparcie”.